package model;

import java.time.LocalTime;

public class Dose
{
    private LocalTime time;
    private double count;

    public Dose(LocalTime time, double count)
    {
        this.time = time;
        this.count = count;
    }

    public double getCount()
    {
        return this.count;
    }

    public void setCount(double count)
    {
        this.count = count;
    }

    public LocalTime getTime()
    {
        return this.time;
    }

    public void setTime(LocalTime time)
    {
        this.time = time;
    }

    @Override
    public String toString()
    {
        return "Time: " + this.time + "   count:  " + this.count;
    }

}
