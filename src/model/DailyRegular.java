package model;

import java.time.LocalDate;

public class DailyRegular extends Prescription
{

    //Link to Dose <#>--> 1.. 0..4
    private Dose[] doses = new Dose[4];

    /**
     * Creates a DailyRegular. Req: startDate not empty, endDate not empty.
     */
    public DailyRegular(LocalDate startDate, LocalDate endDate)
    {
        super(startDate, endDate);
        doses = new Dose[4];
    }

    @Override
    public double totalDose()
    {
        double calculatedDose = 0;
        calculatedDose = this.dose24() * this.dayCount();
        return calculatedDose;
    }

    @Override
    public double dose24()
    {
        double dayDose = 0;
        for (Dose d : doses) {
            dayDose += d.getCount();
        }
        return dayDose;
    }

    public Dose[] getDoses()
    {
        return this.doses;
    }

    public void createDose(double amount, int periodOfDay)
    {
        if (amount >= 0) {
            Dose d = new Dose(null, amount);
            doses[periodOfDay] = d;
        } else {
            throw new IllegalArgumentException("Amount is negative");
        }

    }

    public void deleteDose(Dose dose)
    {
        for (Dose d : doses) {
            if (d.equals(dose)) {
                d = null;
            }
        }
    }

}
