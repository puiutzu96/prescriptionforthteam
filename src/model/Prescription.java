package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public abstract class Prescription
{
    public Prescription(LocalDate startDate, LocalDate endDate)
    {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    //link to Drug --> 0..1
    private Drug drug;

    private LocalDate startDate;
    private LocalDate endDate;

    public LocalDate getStartDate()
    {
        return this.startDate;
    }

    public LocalDate getEndDate()
    {
        return this.endDate;
    }

    //-------------------------------------------------------------------------

    public Drug getDrug()
    {
        return drug;
    }

    //nullable
    public void setDrug(Drug drug)
    {
        this.drug = drug;
    }

    /**
     * Returns the count of days between start date and end date (both
     * inclusive). Requires: startDate <= endDate.
     */
    public long dayCount()
    {
        if (startDate.isAfter(endDate)) {
            throw new IllegalArgumentException();
        }
        return this.startDate.until(this.endDate, ChronoUnit.DAYS) + 1;
    }

    /**
     * Returns the total dose given for the period the prescription is in force.
     */
    public abstract double totalDose();

    /**
     * Returns the average dose given for each day the prescription is used.
     */
    public abstract double dose24();

    @Override
    public String toString()
    {
        return this.startDate + " " + this.endDate + " " + this.drug;
    }

}
