package model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class PN extends Prescription
{

    private Set<LocalDate> datesTaken;

    public PN(LocalDate startDate, LocalDate endDate)
    {
        super(startDate, endDate);
        numberOfTimesGiven = 0;
        datesTaken = new HashSet<LocalDate>();
    }

    private double countOfUnits;
    private int numberOfTimesGiven;

    public double getCountOfUnits()
    {
        return this.countOfUnits;
    }

    public void setCountOfUnits(double count)
    {
        if (count > 0) {
            this.countOfUnits = count;
        } else {
            throw new IllegalArgumentException();
        }
    }

    //-------------------------------------------------------------------------

    /**
     * The prescription is used on the date.
     */
    public void giveDose(LocalDate givesAt)
    {
        if ((givesAt.isBefore(getEndDate()) || givesAt.equals(getEndDate()))
                && (givesAt.isAfter(getStartDate()) || givesAt.equals(getStartDate()))) {

            datesTaken.add(givesAt);
            numberOfTimesGiven++;

        } else {
            throw new IllegalArgumentException("Date is not right");
        }
    }

    /**
     * Returns the number of times this PN prescription is used.
     */
    public int getNumberOfTimesGiven()
    {
        return this.numberOfTimesGiven;
    }

    @Override
    public double totalDose()
    {
        if (countOfUnits >= 0 && numberOfTimesGiven >= 0) {
            double totalDose = 0;
            totalDose += countOfUnits * numberOfTimesGiven;
            return totalDose;
        } else {
            throw new IllegalArgumentException("Values cant be negative");
        }
    }

    public void setNumberOfTimesGiven(int numberOfTimesGiven)
    {
        this.numberOfTimesGiven = numberOfTimesGiven;
    }

    @Override
    public double dose24()
    {
        double dose24 = 0;

        dose24 = totalDose() / datesTaken.size();

        return dose24;
    }
}
