package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DailyIrregular extends Prescription
{
    private ArrayList<Dose> doses;

    public DailyIrregular(LocalDate startDate, LocalDate endDate)
    {
        super(startDate, endDate);
        doses = new ArrayList<Dose>();
    }

    /**
     * Creates a new dose for this irregular prescription.
     */
    public void createDose(LocalTime time, double amount)
    {
        if (amount >= 0) {
            Dose d = new Dose(time, amount);
            doses.add(d);
        } else {
            throw new IllegalArgumentException("Amount is negative");
        }

    }

    public ArrayList<Dose> getDoses()
    {
        return new ArrayList<Dose>(doses);
    }

    public void deleteDose(Dose dose)
    {
        doses.remove(dose);
    }

    @Override
    public double totalDose()
    {
        double amount = 0;
        amount = this.dose24() * this.dayCount();

        return amount;
    }

    @Override
    public double dose24()
    {
        double sum = 0;

        for (Dose d : doses) {
            sum += d.getCount();
        }

        return sum;
    }

}
