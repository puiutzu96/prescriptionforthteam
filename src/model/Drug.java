package model;

public class Drug
{
    private String name;
    private double unitPrKgPrDayLight; // factor to be used if weight of patient < 25 kg
    private double unitPrKgPrDayNormal;// factor to be used if 25 kg <= weight of patient <= 120 kg
    private double unitPrKgPrDayHeavy; // factor to be used if weight of patient > 120 kg
    private String unit;

    /**
     * Creates a drug.
     */
    public Drug(String name, double unitPrKgPrDayLight, double unitPrKgPrDayNormal,
            double unitPrKgPrDayHeavy, String unit)
    {
        this.name = name;
        this.unitPrKgPrDayLight = unitPrKgPrDayLight;
        this.unitPrKgPrDayNormal = unitPrKgPrDayNormal;
        this.unitPrKgPrDayHeavy = unitPrKgPrDayHeavy;
        this.unit = unit;
    }

    public String getUnit()
    {
        return this.unit;
    }

    public String getName()
    {
        return this.name;
    }

    public double getUnitPrKgPrDayLight()
    {
        return this.unitPrKgPrDayLight;
    }

    public double getUnitPrKgPrDayNormal()
    {
        return this.unitPrKgPrDayNormal;
    }

    public double getUnitPrKgPrDayHeavy()
    {
        return this.unitPrKgPrDayHeavy;
    }

    @Override
    public String toString()
    {
        return this.name;
    }
}
