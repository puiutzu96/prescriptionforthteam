package model;

import java.util.ArrayList;

public class Patient
{
    //TODO Link fields and methods

    private String cprNo;
    private String name;
    private double weight;
    //link to Prescription --> 0..*
    private ArrayList<Prescription> prescriptions = new ArrayList<Prescription>();

    /**
     * Creates a new patient. Req: cprNo not empty, name not empty, weight > 0.
     */
    public Patient(String cprNo, String name, double weight)
    {
        this.cprNo = cprNo;
        this.name = name;
        this.weight = weight;
    }

    public ArrayList<Prescription> getPrescriptions()
    {
        return new ArrayList<Prescription>(prescriptions);
    }

    public void addPrescription(Prescription p)
    {
        this.prescriptions.add(p);
    }

    public void removePrescription(Prescription p)
    {
        this.prescriptions.remove(p);
    }

    public String getCprNo()
    {
        return this.cprNo;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getWeight()
    {
        return this.weight;
    }

    public void setWeight(double weight)
    {
        this.weight = weight;
    }

    @Override
    public String toString()
    {
        return this.name + "  " + this.cprNo;
    }

}
