package test;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

import model.PN;

public class PrescriptionTest
{

    @Test
    public void testPrescriptionDayCount()
    {

        LocalDate date1 = LocalDate.of(2016, 02, 10);
        LocalDate date2 = LocalDate.of(2016, 02, 15);

        PN pn1 = new PN(date1, date2);
        PN pn2 = new PN(date1, date1);
        PN pn3 = new PN(date2, date1);

        //--------------------------------------------

        Assert.assertEquals(6, pn1.dayCount());
        Assert.assertEquals(1, pn2.dayCount());

        //--------------------------------------------

        try {
            pn3.dayCount();
        } catch (Exception e) {
            System.out.println("Not working, as expected");
        }

    }

}
