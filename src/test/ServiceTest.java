package test;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Assert;
import org.junit.Test;

import model.DailyRegular;
import model.Drug;
import model.PN;
import model.Patient;
import service.Service;
import storage.Storage;

public class ServiceTest
{

    @Test
    public void testServiceCreatePNPrescription()
    {
        try {
            Patient p = new Patient("Dawg", "Dawgovksi", 60);
            LocalDate date = LocalDate.of(2016, 02, 9);
            Drug aspirin = new Drug(" ", 0, 0, 0, " ");
            Service.createPNPrescription(date, date, p, aspirin, 12);
        } catch (Exception e) {
            Assert.fail();
        }
        try {
            Patient p = new Patient("Dawg", "Dawgovksi", 60);
            LocalDate date = LocalDate.of(2016, 02, 9);
            LocalDate date2 = LocalDate.of(2016, 02, 1);
            Drug aspirin = new Drug(" ", 0, 0, 0, " ");
            Service.createPNPrescription(date, date2, p, aspirin, 12);
        } catch (Exception e) {
            System.out.println("Not working, due to invalid date");
        }

    }

    @Test
    public void testServiceCreateDailyRegularPrescription()
    {
        try {
            double morningCount = 0;
            double noonCount = 30;
            double eveningCount = 10;
            double nightCount = 10;
            Patient p = new Patient("Dawg", "Dawgovksi", 60);
            LocalDate date = LocalDate.of(2016, 02, 9);
            Drug d = new Drug(" ", 0, 0, 0, " ");
            Service.createDailyRegularPrescription(date, date, p, d, morningCount, noonCount,
                    eveningCount, nightCount);
        } catch (Exception e) {
            Assert.fail();
        }
        try {
            double morningCount = 0;
            double noonCount = 30;
            double eveningCount = 10;
            double nightCount = 10;
            Patient p = new Patient("Dawg", "Dawgovksi", 60);
            LocalDate date = LocalDate.of(2016, 02, 9);
            LocalDate date2 = LocalDate.of(2016, 02, 1);
            Drug d = new Drug(" ", 0, 0, 0, " ");
            Service.createDailyRegularPrescription(date, date2, p, d, morningCount, noonCount,
                    eveningCount, nightCount);
        } catch (Exception e) {
            System.out.println("Not working, due to invalid date");
        }

    }

    @Test
    public void testServiceCreateDailyIrregularPrescription()
    {
        try {
            LocalTime hour = LocalTime.of(12, 00);
            LocalTime[] hours = { hour, hour.plusMinutes(40), hour.plusHours(4),
                    LocalTime.of(18, 45) };
            double[] amounts = { 1, 2, 5, 6 };
            Patient p = new Patient("Dawg", "Dawgovksi", 60);
            LocalDate date = LocalDate.of(2016, 02, 9);
            Drug d = new Drug(" ", 0, 0, 0, " ");
            Service.createDailyIrregularPrescription(date, date, p, d, hours, amounts);
        } catch (Exception e) {
            Assert.fail();
        }
        try {
            LocalTime hour = LocalTime.of(12, 00);
            LocalTime[] hours = { hour, hour.plusMinutes(40), hour.plusHours(4),
                    LocalTime.of(18, 45) };
            double[] amounts = { 1, 2, 5, 6 };
            Patient p = new Patient("Dawg", "Dawgovksi", 60);
            LocalDate date = LocalDate.of(2016, 02, 9);
            LocalDate date2 = LocalDate.of(2016, 02, 1);
            Drug d = new Drug(" ", 0, 0, 0, " ");
            Service.createDailyIrregularPrescription(date, date2, p, d, hours, amounts);
        } catch (Exception e) {
            System.out.println("Not working, due to invalid date");
        }

    }

    @Test
    public void testServicePnUsed()
    {
        try {
            LocalDate date = LocalDate.of(2016, 02, 9);//input
            LocalDate startDate = LocalDate.of(2016, 02, 10);
            LocalDate endDate = LocalDate.of(2016, 02, 20);
            PN pn = new PN(startDate, endDate);
            Service.pnUsed(pn, date);
        } catch (Exception e) {
            System.out.println("Not working, due to invalid date");
        }

        try {
            LocalDate date = LocalDate.of(2016, 02, 11);//input
            LocalDate startDate = LocalDate.of(2016, 02, 1);
            LocalDate endDate = LocalDate.of(2016, 02, 10);
            PN pn = new PN(startDate, endDate);
            Service.pnUsed(pn, date);
        } catch (Exception e) {
            System.out.println("Not working, due to invalid date");
        }

        try {
            LocalDate date = LocalDate.of(2016, 02, 10);// input
            LocalDate startDate = LocalDate.of(2016, 02, 1);
            LocalDate endDate = LocalDate.of(2016, 02, 10);
            PN pn = new PN(startDate, endDate);
            Service.pnUsed(pn, date);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void testCreatePatient()
    {
        int oldSize = Service.getAllPatients().size();
        Service.createPatient("cprNo", "Name", 80.0);
        int newSize = Service.getAllPatients().size();
        //--------------------------------------------
        Assert.assertEquals(oldSize + 1, newSize);
    }

    @Test
    public void testCreateDrug()
    {
        int oldSize = Service.getAllDrugs().size();
        Service.createDrug("Name", 1.0, 2.0, 3.0, "ml");
        int newSize = Service.getAllDrugs().size();

        Assert.assertEquals(oldSize + 1, newSize);
    }

    @Test
    public void testServiceCountPrescriptionsPerWeightPerDrug()
    {
        Patient p = new Patient("Dawg", "Dawgovksi", 60);
        Drug aspirin = new Drug(" ", 0, 0, 0, " ");
        LocalDate startDate = LocalDate.of(2016, 02, 10);
        LocalDate endDate = LocalDate.of(2016, 02, 20);
        DailyRegular dr = new DailyRegular(startDate, endDate);
        Drug para = new Drug(" ", 0, 0, 0, " ");
        //--------------------------------------------
        dr.setDrug(aspirin);
        dr.createDose(15, 0);
        dr.createDose(25, 2);
        p.addPrescription(dr);
        Storage.addPatient(p);
        //--------------------------------------------
        Assert.assertEquals(1, Service.countPrescriptionsPerWeightPerDrug(20, 100, aspirin));
        Assert.assertEquals(0, Service.countPrescriptionsPerWeightPerDrug(20, 100, para));
        try {
            Service.countPrescriptionsPerWeightPerDrug(100, 50, aspirin);
        } catch (Exception e) {
            System.out.println("Not working, as expected");
        }
        try {
            Service.countPrescriptionsPerWeightPerDrug(-2, 5, aspirin);
        } catch (Exception e) {
            System.out.println("Not working, as expected");
        }
    }
}
