package test;

import static org.junit.Assert.fail;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

import model.DailyRegular;
import model.Dose;

public class DailyRegularTest
{

    @Test
    public void testCreateDose()
    {
        double amount = 10;
        double amount2 = -10;
        double amount3 = 25;
        int periodOfDay = 0;
        int periodOfDay2 = 2;
        int periodOfDay3 = 3;
        int periodOfDay4 = 4;

        //---------------------------------------------

        DailyRegular dr = new DailyRegular(null, null);
        dr.createDose(amount, periodOfDay);
        dr.createDose(amount3, periodOfDay2);

        Dose[] doses = dr.getDoses();

        int dose = (int) doses[0].getCount();
        int dose2 = (int) doses[2].getCount();

        //---------------------------------------------

        Assert.assertEquals(10, dose, 0.0001);
        Assert.assertEquals(25, dose2, 0.0001);

        //---------------------------------------------

        try {
            dr.createDose(amount2, periodOfDay3);
        } catch (Exception e) {
            System.out.println("Not working due to negative amount");
        }
        try {
            dr.createDose(amount, periodOfDay4);
        } catch (Exception e) {
            System.out.println("Not working due to out of bound error");
        }
    }

    @Test
    public void testTotalDose()
    {
        LocalDate date = LocalDate.of(2016, 1, 02);
        DailyRegular dr = new DailyRegular(date, date.plusDays(2));
        DailyRegular dr2 = new DailyRegular(date, date.plusDays(6));
        DailyRegular dr3 = new DailyRegular(date, date.plusDays(13));

        //---------------------------------------------

        try {
            dr.createDose(20, 0);
            dr.createDose(0, 1);
            dr.createDose(15, 2);
            dr.createDose(0, 3);

            dr2.createDose(20, 0);
            dr2.createDose(65, 1);
            dr2.createDose(35, 2);
            dr2.createDose(0, 3);

            dr3.createDose(15, 0);
            dr3.createDose(0, 1);
            dr3.createDose(15, 2);
            dr3.createDose(0, 3);

        } catch (Exception e) {
            fail();
        }

        //---------------------------------------------

        int res = (int) dr.totalDose();
        int res2 = (int) dr2.totalDose();
        int res3 = (int) dr3.totalDose();

        Assert.assertEquals(105, res);
        Assert.assertEquals(840, res2);
        Assert.assertEquals(420, res3);

    }

    @Test
    public void testDose24()
    {
        LocalDate date = LocalDate.of(2016, 1, 02);
        DailyRegular dr = new DailyRegular(date, date.plusDays(2));
        DailyRegular dr2 = new DailyRegular(date, date.plusDays(6));

        dr.createDose(20, 0);
        dr.createDose(65, 1);
        dr.createDose(35, 2);
        dr.createDose(0, 3);

        dr2.createDose(20, 0);
        dr2.createDose(0, 1);
        dr2.createDose(15, 2);
        dr2.createDose(0, 3);

        int res = (int) dr.dose24();
        int res2 = (int) dr2.dose24();

        //---------------------------------------------

        Assert.assertEquals(120, res);
        Assert.assertEquals(35, res2);

        //---------------------------------------------

    }

}
