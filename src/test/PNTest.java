package test;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

import model.PN;

public class PNTest
{

    @Test
    public void testGiveDose()
    {
        LocalDate date1 = LocalDate.of(2016, 02, 3);
        LocalDate date2 = LocalDate.of(2016, 02, 10);
        LocalDate dateGivesAt1 = LocalDate.of(2016, 02, 4);
        LocalDate dateGivesAt2 = LocalDate.of(2016, 02, 2);
        LocalDate dateGivesAt3 = LocalDate.of(2016, 02, 11);
        LocalDate dateGivesAt4 = LocalDate.of(2016, 02, 3);
        PN pn = new PN(date1, date2);

        //---------------------------------------------

        try {
            pn.giveDose(dateGivesAt2);
        } catch (Exception e) {
            System.out.println("Not working, due to given time");
        }

        try {
            pn.giveDose(dateGivesAt3);
        } catch (Exception e) {
            System.out.println("Not working, due to given time");
        }

        try {
            pn.giveDose(dateGivesAt1);
            pn.giveDose(dateGivesAt4);
        } catch (Exception e) {
            Assert.fail();
        }

    }

    @Test
    public void testTotalDose()
    {
        LocalDate date1 = LocalDate.of(2016, 02, 3);
        LocalDate date2 = LocalDate.of(2016, 02, 10);
        PN pn = new PN(date1, date2);
        pn.setCountOfUnits(10);
        pn.setNumberOfTimesGiven(3);

        //---------------------------------------------

        Assert.assertEquals(30, pn.totalDose(), 0.001);

    }

    @Test
    public void testDose24()
    {
        LocalDate date1 = LocalDate.of(2016, 02, 3);
        LocalDate date2 = LocalDate.of(2016, 02, 10);
        PN pn = new PN(date1, date2);
        pn.setCountOfUnits(10);

        pn.giveDose(date1);
        pn.giveDose(date1.plusDays(1));
        pn.giveDose(date1.plusDays(1));

        //--------------------------------------------

        Assert.assertEquals(15, (int) pn.dose24());

        pn.giveDose(date1.plusDays(2));

        Assert.assertEquals(13, (int) pn.dose24());

    }
}
