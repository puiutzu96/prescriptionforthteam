package test;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Assert;
import org.junit.Test;

import model.DailyIrregular;

public class DailyIrregularTest
{

    @Test
    public void testCreateDose()
    {
        LocalDate date = LocalDate.of(2016, 02, 1);
        LocalTime time = LocalTime.of(13, 0);
        LocalTime time2 = LocalTime.of(0, 0);
        DailyIrregular di = new DailyIrregular(date, date);

        //---------------------------------------------

        int oldSize = di.getDoses().size();
        di.createDose(time, 10);
        int newSize = di.getDoses().size();

        int oldSize2 = di.getDoses().size();
        di.createDose(time2, 50);
        int newSize2 = di.getDoses().size();

        //---------------------------------------------

        Assert.assertEquals(oldSize + 1, newSize);
        Assert.assertEquals(oldSize2 + 1, newSize2);

        //---------------------------------------------

        try {
            di.createDose(LocalTime.of(-13, 0), 10);
        } catch (Exception e) {
            System.out.println("Error due to negative time");
        }
        try {
            di.createDose(LocalTime.of(13, 0), -10);
        } catch (Exception e) {
            System.out.println("Error due to negative amount");
        }

    }

    @Test
    public void testTotalDose()
    {
        LocalDate date = LocalDate.of(2016, 02, 1);
        LocalTime time = LocalTime.of(13, 0);
        LocalTime time2 = LocalTime.of(17, 0);
        DailyIrregular di = new DailyIrregular(date, date.plusDays(2));
        DailyIrregular di2 = new DailyIrregular(date, date.plusDays(6));
        di.createDose(time, 15);
        di.createDose(time2, 22);

        //---------------------------------------------

        int res = (int) di.totalDose();
        int res2 = (int) di2.dose24();

        //---------------------------------------------

        Assert.assertEquals(111, res);
        Assert.assertEquals(0, res2);

    }

    @Test
    public void testDose24()
    {
        LocalDate date = LocalDate.of(2016, 02, 1);
        LocalTime time = LocalTime.of(13, 0);
        LocalTime time2 = LocalTime.of(17, 0);
        DailyIrregular di = new DailyIrregular(date, date.plusDays(2));
        DailyIrregular di2 = new DailyIrregular(date, date.plusDays(6));
        di.createDose(time, 15);
        di.createDose(time2, 22);

        //---------------------------------------------

        int res = (int) di.dose24();
        int res2 = (int) di2.dose24();

        //---------------------------------------------

        Assert.assertEquals(37, res);
        Assert.assertEquals(0, res2);
    }

}
