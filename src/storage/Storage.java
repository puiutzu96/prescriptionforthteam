package storage;

import java.util.ArrayList;
import java.util.List;

import model.Drug;
import model.Patient;

public class Storage
{
    private static List<Patient> patients = new ArrayList<Patient>();
    private List<Drug> drugs = new ArrayList<Drug>();

    /**
     * Returns all patients in storage.
     */
    public List<Patient> getAllPatients()
    {
        return new ArrayList<Patient>(patients);
    }

    /**
     * Adds the patient to storage.
     */
    public static void addPatient(Patient patient)
    {
        patients.add(patient);
    }

    /**
     * Returns all drugs in storage.
     */
    public List<Drug> getAllDrugs()
    {
        return new ArrayList<Drug>(drugs);
    }

    /**
     * Adds the drug to storage.
     */
    public void addDrug(Drug drug)
    {
        drugs.add(drug);
    }

}
