package service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import model.DailyIrregular;
import model.DailyRegular;
import model.Drug;
import model.PN;
import model.Patient;
import model.Prescription;
import storage.Storage;

public class Service
{
    private static Storage storage = new Storage();

    /**
     * Creates and returns a PN prescription.
     *
     * @throws IllegalArgumentException
     *             if start date is after end date.
     */
    public static PN createPNPrescription(LocalDate startDate, LocalDate endDate, Patient patient,
            Drug drug, double count)
    {

        if (startDate.isAfter(endDate)) {
            throw new IllegalArgumentException();
        } else {
            PN prescriptionPn = new PN(startDate, endDate);
            prescriptionPn.setDrug(drug);
            prescriptionPn.setCountOfUnits(count);
            patient.addPrescription(prescriptionPn);
            return prescriptionPn;
        }
    }

    /**
     * Creates and returns a DailyRegular prescription.
     *
     * @throws IllegalArgumentException
     *             if start date is after end date.
     */
    public static DailyRegular createDailyRegularPrescription(LocalDate startDate,
            LocalDate endDate, Patient patient, Drug drug, double morningCount, double noonCount,
            double eveningCount, double nightCount)
    {
        if (startDate.isAfter(endDate)) {
            throw new IllegalArgumentException();
        } else {
            DailyRegular dLprescription = new DailyRegular(startDate, endDate);
            dLprescription.setDrug(drug);
            dLprescription.createDose(morningCount, 0);
            dLprescription.createDose(noonCount, 1);
            dLprescription.createDose(eveningCount, 2);
            dLprescription.createDose(nightCount, 3);
            patient.addPrescription(dLprescription);
            return dLprescription;
        }

    }

    /**
     * Creates and returns a DailyIrregular prescription.
     *
     * @throws IllegalArgumentException
     *             if start date is after end date.
     */
    public static DailyIrregular createDailyIrregularPrescription(LocalDate startDate,
            LocalDate endDate, Patient patient, Drug drug, LocalTime[] hours, double[] counts)
    {
        if (startDate.isAfter(endDate)) {
            throw new IllegalArgumentException();
        } else {
            DailyIrregular irprescription = new DailyIrregular(startDate, endDate);
            irprescription.setDrug(drug);

            for (int i = 0; i < hours.length; i++) {
                irprescription.createDose(hours[i], counts[i]);
            }

            patient.addPrescription(irprescription);
            return irprescription;
        }
    }

    /**
     * Registers the use of the PN prescription on the date.
     *
     * @throws IllegalArgumentException
     *             if the date is not in the time period of the PN prescription.
     */
    public static void pnUsed(PN pn, LocalDate date)
    {
        assert pn != null;
        if (pn.getEndDate().isBefore(date) || pn.getStartDate().isAfter(date)) {
            throw new IllegalArgumentException();
        } else {
            pn.giveDose(date);
        }

    }

    /**
     * Returns the recommended dose for the patient (the dose depends on the
     * weight of the patient).
     */
    public static double recommendedDosePerDay(Patient patient, Drug drug)
    {
        double result = 0;
        if (patient.getWeight() < 25) {
            result = patient.getWeight() * drug.getUnitPrKgPrDayLight();
        } else if (patient.getWeight() > 120) {
            result = patient.getWeight() * drug.getUnitPrKgPrDayHeavy();
        } else {
            result = patient.getWeight() * drug.getUnitPrKgPrDayNormal();
        }
        return result;
    }

    /**
     * Creates a patient. Req: cprNo not empty, name not empty, weight >= 0.
     */
    public static Patient createPatient(String cprNo, String name, double weight)
    {
        assert cprNo != null;
        assert name != null;
        assert weight >= 0;

        Patient patient = new Patient(cprNo, name, weight);
        Storage.addPatient(patient);
        return patient;
    }

    /**
     * Creates a drug. Requires: name not empty, unitPrKgPrDaylight >= 0,
     * unitPrKgPrDayNormal >= 0, unitPrKgPrDayHeavy >= 0, unit in ["items",
     * "inhalations", "mL", "drops"].
     */
    public static Drug createDrug(String name, double unitPrKgPrDayLight,
            double unitPrKgPrDayNormal, double unitPrKgPrDayHeavy, String unit)
    {
        assert name != null;
        assert unitPrKgPrDayLight >= 0;
        assert unitPrKgPrDayNormal >= 0;
        assert unitPrKgPrDayHeavy >= 0;

        Drug drug = new Drug(name, unitPrKgPrDayLight, unitPrKgPrDayNormal, unitPrKgPrDayHeavy,
                unit);
        storage.addDrug(drug);
        return drug;
    }

    /**
     * Returns the count of prescriptions for the specified weight interval and
     * the specified drug.
     */
    public static int countPrescriptionsPerWeightPerDrug(double weightStart, double weightEnd,
            Drug drug)
    {
        int amount = 0;

        if (weightStart < weightEnd || weightStart >= 0 || weightEnd > 0) {
            for (Patient p : storage.getAllPatients()) {
                if (p.getWeight() >= weightStart && p.getWeight() <= weightEnd) {
                    for (Prescription pr : p.getPrescriptions()) {
                        if (pr.getDrug().equals(drug)) {
                            amount++;
                        }

                    }
                }
            }
        } else {
            throw new IllegalArgumentException();
        }

        return amount;
    }

    public static List<Patient> getAllPatients()
    {
        return storage.getAllPatients();
    }

    public static List<Drug> getAllDrugs()
    {
        return storage.getAllDrugs();
    }

    /**
     * Initializes storage.
     */
    public static void initStorage()
    {
        Patient p1 = Service.createPatient("121256-0512", "Jane Jensen", 63.4);
        Patient p2 = Service.createPatient("070985-1153", "Finn Madsen", 83.2);
        Service.createPatient("050972-1233", "Hans Jørgensen", 89.4);
        Patient p4 = Service.createPatient("011064-1522", "Ulla Nielsen", 59.9);
        Service.createPatient("090149-2529", "Ib Hansen", 87.7);

        Drug d1 = Service.createDrug("Pinex", 0.1, 0.15, 0.16, "pieces");
        Drug d2 = Service.createDrug("Paracetamol", 1, 1.5, 2, "mL");
        Drug d3 = Service.createDrug("ABC", 1, 15, 2, "drops");
        Service.createDrug("Fucidin", 0.025, 0.025, 0.025, "pieces");

        Service.createPNPrescription(LocalDate.of(2016, 3, 1), LocalDate.of(2016, 3, 12), p1, d1,
                123);
        Service.createPNPrescription(LocalDate.of(2016, 4, 12), LocalDate.of(2016, 4, 12), p1, d2,
                3);
        Service.createPNPrescription(LocalDate.of(2016, 3, 1), LocalDate.of(2016, 3, 12), p4, d3,
                5);

        Service.createDailyRegularPrescription(LocalDate.of(2016, 1, 20), LocalDate.of(2016, 1, 25),
                p2, d2, 2, 0, 1, 0);

        LocalTime[] hours = { LocalTime.of(12, 00), LocalTime.of(12, 40), LocalTime.of(16, 00),
                LocalTime.of(18, 45) };
        double[] drops = { 1, 2, 5, 6 };
        Service.createDailyIrregularPrescription(LocalDate.of(2016, 1, 23),
                LocalDate.of(2016, 1, 24), p2, d3, hours, drops);
    }
}
