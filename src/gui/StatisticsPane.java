package gui;

import model.Drug;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import service.Service;

public class StatisticsPane extends GridPane
{
    private TextField prescriptionsPerWeightPerDrug = new TextField();
    private TextField txfWeightFrom = new TextField();
    private TextField txfWeightTo = new TextField();
    private ComboBox<Drug> lstDrugs = new ComboBox<Drug>();

    public StatisticsPane()
    {
        this.setPadding(new Insets(20));
        this.setHgap(20);
        this.setVgap(10);
        this.setGridLinesVisible(false);
        this.initContent();
    }

    private void initContent()
    {
        GridPane pane1 = new GridPane();
        pane1.setHgap(20);
        pane1.setVgap(10);
        pane1.setPadding(new Insets(10));

        GridPane pane2 = new GridPane();
        pane2.setHgap(20);
        pane2.setVgap(10);
        pane2.setPadding(new Insets(10));

        pane1.setStyle("-fx-border-color: grey;");
        pane2.setStyle("-fx-border-color: grey;");

        Label label = new Label("Count of Drugs");
        label.setFont(new Font(25));
        this.add(label, 0, 0, 2, 1);

        this.txfWeightFrom.setMaxWidth(40);
        this.txfWeightTo.setMaxWidth(40);
        pane1.add(new Label("Weight from: "), 0, 0);
        pane1.add(this.txfWeightFrom, 1, 0);

        pane1.add(new Label("Weight to: "), 0, 1);
        pane1.add(this.txfWeightTo, 1, 1);

        this.lstDrugs.getItems().setAll(Service.getAllDrugs());
        pane1.add(new Label("Drug: "), 0, 2);
        pane1.add(this.lstDrugs, 1, 2);
        this.add(pane1, 0, 1);

        pane2.add(new Label("Count: "), 0, 0);
        this.prescriptionsPerWeightPerDrug.setEditable(false);
        pane2.add(this.prescriptionsPerWeightPerDrug, 1, 0);
        this.add(pane2, 0, 2);

        // Adding listeners
        this.txfWeightFrom.setOnKeyReleased(event -> this.updateDetails());
        this.txfWeightTo.setOnKeyReleased(event -> this.updateDetails());
        this.lstDrugs.setOnAction(event -> this.updateDetails());

        this.updateDetails();
    }

    public void updateDetails()
    {
        try {
            int vFrom = Integer.valueOf(this.txfWeightFrom.getText());
            int vTo = Integer.valueOf(this.txfWeightTo.getText());
            Drug drug = this.lstDrugs.getSelectionModel().getSelectedItem();
            int count = Service.countPrescriptionsPerWeightPerDrug(vFrom, vTo, drug);
            this.prescriptionsPerWeightPerDrug.setText(count + "");
        }
        catch (NumberFormatException e) {
            this.prescriptionsPerWeightPerDrug.setText("");
        }
    }

}
