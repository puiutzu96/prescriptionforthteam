package gui;

import java.time.LocalTime;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Drug;
import model.Patient;
import service.Service;

public class CreatePrescriptionDialog extends Stage
{

    private final Patient patient;
    private final Drug drug;
    private final TypePrescription type;

    private DatePicker startDate = new DatePicker();
    private DatePicker endDate = new DatePicker();
    private TextField txtUnits = new TextField();

    private Button btnCreate = new Button("Create");
    private Button btnCancel = new Button("Cancel");

    private Label lblError = new Label();
    private DailyRegularPane dailyRegularPane;
    private DailyIrregularPane dailyIrregularPane;

    public CreatePrescriptionDialog(Patient patient, Drug drug, TypePrescription type)
    {
        this.patient = patient;
        this.drug = drug;
        this.type = type;

        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);
        this.setTitle("Prescript drug!");

        GridPane pane = new GridPane();
        Scene scene = new Scene(pane);
        this.initContent(pane);
        this.setScene(scene);
    }

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(20));
        pane.setHgap(20);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        pane.add(new Label("Patient: " + this.patient), 0, 0, 2, 1);
        pane.add(new Label("Drug:"), 0, 1);
        pane.add(new Label(this.drug + ""), 1, 1);

        pane.add(new Label("Startdate:"), 0, 2);
        pane.add(this.startDate, 1, 2);
        pane.add(new Label("Enddate:"), 0, 3);
        pane.add(this.endDate, 1, 3);

        pane.add(new Label("Recomm. day dose: "), 0, 4, 2, 1);
        String count = String.format("%.2f", Service.recommendedDosePerDay(this.patient, this.drug))
                + " " + this.drug.getUnit();
        Label lblCount = new Label(count);
        pane.add(lblCount, 1, 4);
        GridPane.setHalignment(lblCount, HPos.CENTER);

        if (this.type.equals(TypePrescription.PN)) {
            pane.add(new Label("Day dose:"), 0, 5);
            pane.add(this.txtUnits, 1, 5);
        } else if (this.type.equals(TypePrescription.REGULAR)) {
            this.dailyRegularPane = new DailyRegularPane(350);
            pane.add(this.dailyRegularPane, 0, 5, 2, 1);
        } else if (this.type.equals(TypePrescription.IRREGULAR)) {
            this.dailyIrregularPane = new DailyIrregularPane();
            pane.add(this.dailyIrregularPane, 0, 5, 2, 1);
        }

        this.btnCreate.setMinWidth(100);
        this.btnCreate.setOnAction(event -> this.createAction());
        this.btnCancel.setMinWidth(100);
        this.btnCancel.setOnAction(event -> this.hide());

        HBox hbox = new HBox(10);
        hbox.alignmentProperty().set(Pos.BOTTOM_CENTER);
        hbox.getChildren().add(this.btnCreate);
        hbox.getChildren().add(this.btnCancel);
        pane.add(hbox, 0, 7, 2, 1);

        this.lblError.setTextFill(Color.RED);
        pane.add(this.lblError, 0, 8, 2, 1);
    }

    private void createAction()
    {
        if (this.type.equals(TypePrescription.PN)) {
            this.createPN();
        } else if (this.type.equals(TypePrescription.REGULAR)) {
            this.createRegular();
        } else if (this.type.equals(TypePrescription.IRREGULAR)) {
            this.createIrregular();
        }
    }

    private void createPN()
    {
        if (this.startDate.getValue() == null || this.endDate.getValue() == null) {
            this.lblError.setText("Dates must be given");
            return;
        }
        double dose = -1;
        try {
            dose = Double.parseDouble(this.txtUnits.getText());
        } catch (NumberFormatException ex) {
            this.lblError.setText("Dose must be a number");
            return;
        }
        if (dose <= 0) {
            this.lblError.setText("Dose must be > 0");
            return;
        }

        try {
            Service.createPNPrescription(this.startDate.getValue(), this.endDate.getValue(),
                    this.patient, this.drug, dose);
        } catch (IllegalArgumentException e) {
            this.lblError.setText(e.getMessage());
            return;
        }
        this.hide();
    }

    public void createRegular()
    {
        double[] doses = { 0, 0, 0, 0 };

        if (this.startDate.getValue() == null || this.endDate.getValue() == null) {
            this.lblError.setText("Dates must be given");
            return;
        }

        try {
            this.parseField(this.dailyRegularPane.getMorning(), doses, 0);
            this.parseField(this.dailyRegularPane.getNoon(), doses, 1);
            this.parseField(this.dailyRegularPane.getEvening(), doses, 2);
            this.parseField(this.dailyRegularPane.getNight(), doses, 3);

            Service.createDailyRegularPrescription(this.startDate.getValue(),
                    this.endDate.getValue(), this.patient, this.drug, doses[0], doses[1], doses[2],
                    doses[3]);
        } catch (IllegalArgumentException e) {
            this.lblError.setText(e.getMessage());
            return;
        }

        this.hide();
    }

    private void parseField(Double field, double[] doser, int index)
    {
        if (field != null) {
            double dose = field;
            if (dose >= 0) {
                doser[index] = dose;
            } else {
                throw new IllegalArgumentException("Dose must be >= 0");
            }
        }
    }

    private void createIrregular()
    {
        if (this.startDate.getValue() == null || this.endDate.getValue() == null) {
            this.lblError.setText("Dates must be given");
            return;
        }

        String[] doses = this.dailyIrregularPane.getDosisArray();
        try {
            if (doses.length == 0) {
                throw new IllegalArgumentException("No dose is given");
            }
            Service.createDailyIrregularPrescription(this.startDate.getValue(),
                    this.endDate.getValue(), this.patient, this.drug, this.createTime(doses),
                    this.createCount(doses));
        } catch (IllegalArgumentException e) {
            this.lblError.setText(e.getMessage());
            return;
        }
        this.hide();
    }

    private LocalTime[] createTime(String[] model)
    {
        LocalTime[] resultat = new LocalTime[model.length];
        try {
            for (int i = 0; i < model.length; i++) {
                resultat[i] = LocalTime.parse(model[i].substring(0, 5));
            }
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("Time is not correct");
        }
        return resultat;
    }

    private double[] createCount(String[] model)
    {
        double[] result = new double[model.length];
        try {
            for (int i = 0; i < model.length; i++) {
                double dosis = Double.parseDouble((model[i]).substring(6));
                result[i] = dosis;
            }
        } catch (RuntimeException e) {
            throw new IllegalArgumentException("Correct count is not given");
        }
        return result;
    }

}
