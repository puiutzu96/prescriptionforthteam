package gui;

import java.time.LocalDate;

import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import model.DailyIrregular;
import model.Dose;
import model.PN;
import model.Prescription;
import service.Service;

public class PrescriptionDetailsPane extends GridPane
{
    private TextField txtStartTime, txtEndTime, txtDrug, txtDaydose, txtTotalDose;

    // Daily Regular
    private DailyRegularPane regularPane = new DailyRegularPane(280);

    // Daily Irregular
    private TextArea textAreaIrregular = new TextArea();

    // PN
    private GridPane pnPane = new GridPane();
    private TextField txtDose = new TextField();
    private TextField txtUsed = new TextField();
    private Button btnUse = new Button("Use Prescription");
    private DatePicker datePicker = new DatePicker();
    private PN pn;
    private Label lblError = new Label();

    public PrescriptionDetailsPane()
    {
        this.setHgap(20);
        this.setVgap(10);
        this.setGridLinesVisible(false);

        this.txtStartTime = new TextField();
        this.txtStartTime.setEditable(false);
        this.txtEndTime = new TextField();
        this.txtEndTime.setEditable(false);
        this.txtDrug = new TextField();
        this.txtDrug.setEditable(false);
        this.txtDaydose = new TextField();
        this.txtDaydose.setEditable(false);
        this.txtTotalDose = new TextField();
        this.txtTotalDose.setEditable(false);

        this.add(new Label("Start time"), 0, 1);
        this.add(new Label("End time"), 0, 2);
        this.add(new Label("Drug"), 0, 3);
        this.add(new Label("Day dose"), 0, 4);
        this.add(new Label("Total dose"), 0, 5);

        this.add(this.txtStartTime, 1, 1);
        this.add(this.txtEndTime, 1, 2);
        this.add(this.txtDrug, 1, 3);
        this.add(this.txtDaydose, 1, 4);
        this.add(this.txtTotalDose, 1, 5);

        // Daily Regular
        this.regularPane.makeReadOnly();

        // Daily Irregular
        this.textAreaIrregular.setMaxWidth(250);
        this.textAreaIrregular.setEditable(false);

        // PN
        this.pnPane.setHgap(20);
        this.pnPane.setVgap(10);
        this.pnPane.setGridLinesVisible(false);
        this.pnPane.add(new Label("Dose"), 0, 1);
        this.pnPane.add(new Label("Given"), 0, 2);
        this.pnPane.add(this.txtDose, 1, 1);
        this.pnPane.add(this.txtUsed, 1, 2);
        this.pnPane.add(this.datePicker, 0, 3);
        this.pnPane.add(this.btnUse, 1, 3);
        this.datePicker.setMaxWidth(150);

        this.lblError.setTextFill(Color.RED);
        pnPane.add(this.lblError, 0, 4, 2, 1);

        this.btnUse.setOnAction(event -> this.actionUse());
    }

    private void actionUse()
    {
        LocalDate usedDato = this.datePicker.getValue();
        if (usedDato == null) {
            lblError.setText("Date must be given");
            return;
        }

        try {
            Service.pnUsed(this.pn, usedDato);
        } catch (IllegalArgumentException ex) {
            lblError.setText(ex.getMessage());
            return;
        }

        this.txtUsed.setText(this.pn.getNumberOfTimesGiven() + " times");
        this.txtDose.setText(this.pn.getCountOfUnits() + "");

        this.txtDaydose.setText(this.pn.dose24() + " " + this.pn.getDrug().getUnit());
        this.txtTotalDose.setText(this.pn.totalDose() + " " + this.pn.getDrug().getUnit());

        lblError.setText("");
    }

    public void clear()
    {
        this.txtStartTime.clear();
        this.txtEndTime.clear();
        this.txtDrug.clear();
        this.txtDaydose.clear();
        this.txtTotalDose.clear();
        this.getChildren().remove(this.regularPane);
        this.getChildren().remove(this.textAreaIrregular);
        this.getChildren().remove(this.pnPane);
    }

    public void setPrescription(Prescription prescription)
    {
        this.txtStartTime.setText(prescription.getStartDate().toString());
        this.txtEndTime.setText(prescription.getEndDate().toString());
        this.txtDrug.setText(prescription.getDrug().toString());
        this.txtDaydose.setText(prescription.dose24() + "");
        this.txtTotalDose.setText(prescription.totalDose() + "");
    }

    public void setRegular(Dose morning, Dose noon, Dose evening, Dose night)
    {
        this.add(this.regularPane, 0, 6, 2, 1);
        if (morning != null) {
            this.regularPane.setMorning(morning.getCount() + "");
        }
        if (noon != null) {
            this.regularPane.setNoon(noon.getCount() + "");
        }
        if (evening != null) {
            this.regularPane.setEvening(evening.getCount() + "");
        }
        if (night != null) {
            this.regularPane.setNight(night.getCount() + "");
        }
    }

    public void setIrregular(DailyIrregular irregular)
    {
        this.textAreaIrregular.clear();
        this.add(this.textAreaIrregular, 0, 6, 2, 1);
        for (Dose d : irregular.getDoses()) {
            this.textAreaIrregular.appendText(d.toString() + "\n");
        }
    }

    public void setPN(PN pn)
    {
        this.pn = pn;
        this.add(this.pnPane, 0, 6, 2, 1);
        this.txtDose.setText(pn.getCountOfUnits() + "");
        this.txtUsed.setText(pn.getNumberOfTimesGiven() + " times");
    }

}
