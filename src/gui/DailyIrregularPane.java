package gui;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class DailyIrregularPane extends GridPane
{
    private TextField txtTimeMinute = new TextField();
    private TextField txtCount = new TextField();
    private Button btnCreate = new Button("Create Dose");
    private ListView<String> listDoses = new ListView<>();

    public DailyIrregularPane()
    {
        this.setHgap(20);
        this.setVgap(10);
        this.setGridLinesVisible(false);

        this.txtTimeMinute.setPromptText("TT:MM");
        this.txtCount.setPromptText("Count");

        HBox hbox = new HBox(8);
        hbox.getChildren().add(this.txtTimeMinute);
        hbox.getChildren().add(this.txtCount);
        hbox.getChildren().add(this.btnCreate);
        this.add(hbox, 0, 0);

        this.listDoses.setMaxHeight(100);
        this.add(this.listDoses, 0, 1);

        this.btnCreate.setOnAction(event -> this.createDoses());
    }

    private void createDoses()
    {
        String dosis = this.txtTimeMinute.getText() + " " + this.txtCount.getText();
        dosis.trim();
        if (dosis.length() <= 1) {
            return;
        }

        this.listDoses.getItems().add(dosis);
    }

    public String[] getDosisArray()
    {
        ObservableList<String> items = this.listDoses.getItems();
        return items.toArray(new String[items.size()]);
    }
}
