package gui;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class DailyRegularPane extends GridPane
{
    private TextField txtMorning = new TextField();
    private TextField txtNoon = new TextField();
    private TextField txtEvening = new TextField();
    private TextField txtNight = new TextField();

    public DailyRegularPane(int maxWidth)
    {
        this.setHgap(20);
        this.setVgap(10);
        this.setGridLinesVisible(false);
        this.setMaxWidth(maxWidth);

        this.add(new Label("Morning"), 0, 0);
        this.add(new Label("Noon"), 1, 0);
        this.add(new Label("Evening"), 2, 0);
        this.add(new Label("Night"), 3, 0);
        this.add(this.txtMorning, 0, 1);
        this.add(this.txtNoon, 1, 1);
        this.add(this.txtEvening, 2, 1);
        this.add(this.txtNight, 3, 1);
    }

    public void setMorning(String morning)
    {
        this.txtMorning.setText(morning);
    }

    public void setNoon(String noon)
    {
        this.txtNoon.setText(noon);
    }

    public void setEvening(String evening)
    {
        this.txtEvening.setText(evening);
    }

    public void setNight(String night)
    {
        this.txtNight.setText(night);
    }

    public Double getMorning()
    {
        return this.parseTextField(this.txtMorning);
    }

    public Double getNoon()
    {
        return this.parseTextField(this.txtNoon);
    }

    public Double getEvening()
    {
        return this.parseTextField(this.txtEvening);
    }

    public Double getNight()
    {
        return this.parseTextField(this.txtNight);
    }

    private Double parseTextField(TextField textField)
    {
        if (textField.getText().isEmpty()) {
            throw new IllegalArgumentException("Dose is empty");
        } else {
            try {
                Double d = Double.parseDouble(textField.getText());
                return d;
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException("Dose must be a number");
            }
        }
    }

    public void makeReadOnly()
    {
        this.txtMorning.setEditable(false);
        this.txtNoon.setEditable(false);
        this.txtEvening.setEditable(false);
        this.txtNight.setEditable(false);
    }
}
