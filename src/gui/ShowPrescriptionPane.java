package gui;

import model.DailyIrregular;
import model.DailyRegular;
import model.PN;
import model.Patient;
import model.Prescription;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import service.Service;

public class ShowPrescriptionPane extends GridPane
{
    private ListView<Patient> lstPatient = new ListView<>();
    private ListView<Prescription> lstPrescription = new ListView<>();
    private PrescriptionDetailsPane prescriptionDetailsPane = new PrescriptionDetailsPane();

    public ShowPrescriptionPane()
    {
        this.setPadding(new Insets(20));
        this.setHgap(20);
        this.setVgap(10);
        this.setGridLinesVisible(false);

        this.add(new Label("Choose Patient"), 0, 0);
        this.add(this.lstPatient, 0, 1);
        this.lstPatient.getItems().setAll(Service.getAllPatients());
        this.lstPatient
            .getSelectionModel()
            .selectedIndexProperty()
            .addListener(
                observable -> {
                    this.lstPrescription.getItems().setAll(
                        this.lstPatient.getSelectionModel().getSelectedItem().getPrescriptions());
                });
        this.lstPatient.getSelectionModel().selectFirst();

        this.add(new Label("Choose Prescription"), 1, 0);
        this.add(this.lstPrescription, 1, 1);
        this.lstPrescription.getSelectionModel().selectedItemProperty().addListener(observable -> {
            this.updateDetails();
        });

        this.add(new Label("Prescription Details"), 2, 0);
        this.add(this.prescriptionDetailsPane, 2, 1);
    }

    public void updateDetails()
    {
        Prescription prescription = this.lstPrescription.getSelectionModel().getSelectedItem();
        this.prescriptionDetailsPane.clear();
        if (prescription != null) {
            this.prescriptionDetailsPane.clear();
            this.prescriptionDetailsPane.setPrescription(prescription);
            if (prescription instanceof DailyRegular) {
                DailyRegular dailyRegular = (DailyRegular) prescription;
                this.prescriptionDetailsPane.setRegular(
                    dailyRegular.getDoses()[0],
                    dailyRegular.getDoses()[1],
                    dailyRegular.getDoses()[2],
                    dailyRegular.getDoses()[3]);
            }
            else if (prescription instanceof DailyIrregular) {
                this.prescriptionDetailsPane.setIrregular((DailyIrregular) prescription);
            }
            else if (prescription instanceof PN) {
                this.prescriptionDetailsPane.setPN((PN) prescription);
            }
        }
    }

    public void updateControls()
    {
        int selected = 0;
        if (this.lstPrescription.getSelectionModel().getSelectedItem() != null) {
            selected = this.lstPrescription.getSelectionModel().getSelectedIndex();
        }
        this.lstPrescription.getItems().setAll(
            this.lstPatient.getSelectionModel().getSelectedItem().getPrescriptions());
        this.lstPrescription.getSelectionModel().select(selected);
    }
}
