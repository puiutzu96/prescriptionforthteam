package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import service.Service;

public class MainApp extends Application
{
    public static void main(String[] args)
    {
        Application.launch(args);
    }

    @Override
    public void init()
    {
        Service.initStorage();
    }

    @Override
    public void start(Stage stage)
    {
        stage.setTitle("Drug Prescription");
        BorderPane pane = new BorderPane();
        initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.setHeight(500);
        stage.setWidth(850);
        stage.show();
    }

    private void initContent(BorderPane pane)
    {
        TabPane tabPane = new TabPane();
        initTabPane(tabPane);
        pane.setCenter(tabPane);
    }

    private void initTabPane(TabPane tabPane)
    {
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

        Tab tabCreate = new Tab("Create Prescriptions");
        Tab tabShow = new Tab("Show Prescriptions");
        Tab tabStatistics = new Tab("Show Statistics");

        CreatePrescriptionPane createPane = new CreatePrescriptionPane();
        tabCreate.setContent(createPane);
        ShowPrescriptionPane showPane = new ShowPrescriptionPane();
        tabShow.setContent(showPane);
        StatisticsPane statPane = new StatisticsPane();
        tabStatistics.setContent(statPane);

        tabPane.getTabs().add(tabCreate);
        tabPane.getTabs().add(tabShow);
        tabPane.getTabs().add(tabStatistics);

        tabShow.setOnSelectionChanged(event -> showPane.updateControls());
    }
}
