package gui;

import model.Drug;
import model.Patient;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import service.Service;

public class CreatePrescriptionPane extends GridPane
{
    private ListView<Patient> lstPatient = new ListView<>();
    private ListView<Drug> lstDrug = new ListView<>();
    private ToggleGroup toggleGroup = new ToggleGroup();
    private RadioButton rbPN = new RadioButton("PN");
    private RadioButton rbRegular = new RadioButton("Daily Regular");
    private RadioButton rbIrregular = new RadioButton("Daily Irregular");
    private Button btnCreate = new Button("Create Prescription");
    private Label lblError;

    public CreatePrescriptionPane()
    {
        this.setPadding(new Insets(20));
        this.setHgap(20);
        this.setVgap(10);
        this.setGridLinesVisible(false);

        this.lblError = new Label();
        this.lblError.setTextFill(Color.RED);

        this.rbPN.setUserData(TypePrescription.PN);
        this.rbIrregular.setUserData(TypePrescription.IRREGULAR);
        this.rbRegular.setUserData(TypePrescription.REGULAR);
        this.toggleGroup.getToggles().add(this.rbPN);
        this.toggleGroup.getToggles().add(this.rbIrregular);
        this.toggleGroup.getToggles().add(this.rbRegular);

        this.add(new Label("Choose Patient"), 0, 0);
        this.add(this.lstPatient, 0, 1, 1, 2);
        this.lstPatient.getItems().setAll(Service.getAllPatients());

        this.add(new Label("Choose Drug"), 1, 0);
        this.add(this.lstDrug, 1, 1, 1, 2);
        this.lstDrug.getItems().setAll(Service.getAllDrugs());

        this.add(new Label("Choose Prescription"), 2, 0);
        GridPane innerPane = new GridPane();
        innerPane.setVgap(10);
        innerPane.add(this.rbPN, 0, 0);
        innerPane.add(this.rbIrregular, 0, 1);
        innerPane.add(this.rbRegular, 0, 2);

        this.add(innerPane, 2, 1);

        this.add(this.lblError, 0, 3, 1, 2);
        this.add(this.btnCreate, 2, 2);
        this.btnCreate.setOnAction(event -> this.actionCreate());

        RowConstraints row1 = new RowConstraints();
        RowConstraints row2 = new RowConstraints();
        RowConstraints row3 = new RowConstraints();
        row3.setValignment(VPos.BOTTOM);
        this.getRowConstraints().addAll(row1, row2, row3);
    }

    public void actionCreate()
    {
        this.lblError.setText("");
        if (this.lstPatient.getSelectionModel().getSelectedItem() == null) {
            this.lblError.setText("Choose a patient");
        }
        else if (this.lstDrug.getSelectionModel().getSelectedItem() == null) {
            this.lblError.setText("Choose a drug");
        }
        else if (this.toggleGroup.getSelectedToggle() == null) {
            this.lblError.setText("Choose a prescription type");
        }
        else {
            CreatePrescriptionDialog dia = new CreatePrescriptionDialog(
                this.lstPatient.getSelectionModel().getSelectedItem(),
                this.lstDrug.getSelectionModel().getSelectedItem(),
                (TypePrescription) this.toggleGroup.getSelectedToggle().getUserData());
            dia.showAndWait();
        }
    }
}
